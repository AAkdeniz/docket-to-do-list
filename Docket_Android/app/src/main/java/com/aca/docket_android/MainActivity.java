package com.aca.docket_android;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";
    public static final String EXTRA_INDEX = "index";
    public static final String FILE_NAME = "data.txt";

    public static ArrayList<Todo> todoList = new ArrayList<>();

    ListView lvTodos;
    public static ArrayAdapter<Todo> todoAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Call initializer
        initializer();
    }

    private void initializer(){
        lvTodos = (ListView) findViewById(R.id.lvTodos);
        todoAdapter = new TodoArrayAdapter(this, todoList);
        lvTodos.setAdapter(todoAdapter);
        lvTodos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // When to-do clicked get selected one
                Todo todoClicked = (Todo) parent.getItemAtPosition(position);
                int index = todoList.indexOf(todoClicked);
                Intent intent = new Intent(MainActivity.this, AddEditActivity.class);
                intent.putExtra(EXTRA_INDEX, index);
                startActivity(intent);
            }
        });
     }

    // Call load, when the activity enters the Started state, the system invokes this callback
    @Override
    public void onStart() {
        super.onStart();
        loadDataFromFile();
        Collections.sort(todoList, new Comparator<Todo>() {
            @Override
            public int compare(Todo todo1, Todo todo2) {
                return Boolean.compare(todo2.isStarred, todo1.isStarred);
            }
        });
        //Reaload the list view
        todoAdapter.notifyDataSetChanged();
    }

    //Save, when activity is no longer visible to the user, it has entered the Stopped state, and the system invokes the onStop() callback
    @Override
    public void onStop() {
        super.onStop();
        saveDataToFile();
    }

    //Save, From the Stopped state, the activity either comes back to interact with the user, or the activity is finished running and goes away. If the activity comes back, the system invokes onRestart()
    @Override
    public void onRestart() {
        super.onRestart();
        saveDataToFile();
    }

    //Create men, onCreateOptionsMenu() to specify the options menu for an activity. In this method, you can inflate your menu resource (defined in XML) into the Menu provided in the callback.
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    //When the user selects an item from the options menu (including action items in the app bar), the system calls your activity's onOptionsItemSelected() method. This method passes the MenuItem selected.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mi_add: {
                Intent intent = new Intent(this, AddEditActivity.class);
                startActivity(intent);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    //Load the data
    private void loadDataFromFile() {
        FileInputStream fileInputStream = null;
        Scanner fileInput = null;
        try {
            fileInputStream = openFileInput(FILE_NAME);
            fileInput = new Scanner(fileInputStream);
            todoList.clear();
            while (fileInput.hasNextLine()) {
                try {
                    String line = fileInput.nextLine();
                    String[] data = line.split(";");
                    String title = data[0];
                    Date dueDate = dateFormat.parse(data[1]);
                    boolean isDone = data[2].equals("done");
                    boolean isStarred = data[3].equals("starred");

                    Todo todo = new Todo(title, dueDate, isDone, isStarred);
                    todoList.add(todo);

                } catch (ParseException ex) {
                    Log.e(TAG, "Error parsing data from file", ex); // NOTE: 3rd parameter is the Exception !
                    Toast.makeText(this, "Error parsing data from file", Toast.LENGTH_LONG).show();
                }
            }
            todoAdapter.notifyDataSetChanged();
            Toast.makeText(this, "Data loaded from file", Toast.LENGTH_LONG).show();
        } catch (FileNotFoundException ex) {
            // ignore if file does not exist yet
            Log.d(TAG, "Data file not found, probably first use");
        } catch (NoSuchElementException | IllegalStateException ex) {
            Log.e(TAG, "Error loading data from file", ex);
            Toast.makeText(this, "Error loading data from file", Toast.LENGTH_LONG).show();
        } finally {
            if (fileInput != null) {
                fileInput.close();
            }
        }
    }

    //Save the data
    private void saveDataToFile() {
        FileOutputStream fileOutputStream = null;
        PrintWriter printWriter = null;
        try {
            fileOutputStream = openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
            printWriter = new PrintWriter(fileOutputStream);
            for (Todo todo : todoList) {
                String dateStr = dateFormat.format(todo.dueDate);
                String isStarredStr = todo.isStarred ? "starred" : "not_starred";
                //Save as String
                printWriter.printf("%s;%s;%s;%s\n", todo.title, dateStr, todo.isDone ? "done" : "pending", isStarredStr );
            }
            Log.d(TAG, "File contents written");
        } catch (IOException ex) {
            Log.e(TAG, "Error saving data to file", ex ); // NOTE: 3rd parameter is the Exception !
            Toast.makeText(this, "Error saving text to file", Toast.LENGTH_LONG).show();
        } finally {
            if (printWriter != null) {
                printWriter.close();
            }
        }
    }

    public class TodoArrayAdapter extends ArrayAdapter<Todo> {

        private Context context;
        private ArrayList<Todo> list;

        public TodoArrayAdapter(Context context, ArrayList<Todo> list) {
            super(context, R.layout.todo_item, list);
            this.context = context;
            this.list = list;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View rowView = inflater.inflate(R.layout.todo_item, parent, false);
            TextView tvFirstLine = (TextView) rowView.findViewById(R.id.tvFirstLine);
            TextView tvSecondLine = (TextView) rowView.findViewById(R.id.tvSecondLine);
            ImageView ivIconIsDone = (ImageView) rowView.findViewById(R.id.ivIconIsDone);
            ImageView ivIconIsStarred = (ImageView) rowView.findViewById(R.id.ivIconIsStarred);

            Todo todo = list.get(position);
            tvFirstLine.setText(todo.title);

            strikeThrough(todo, tvFirstLine);

            String date = dateFormat.format(todo.dueDate);

            tvSecondLine.setText(date);

            ifToday(todo,tvSecondLine);

            ivIconIsDone.setImageResource(todo.isDone ? android.R.drawable.checkbox_on_background : android.R.drawable.checkbox_off_background);
            ivIconIsStarred.setImageResource(todo.isStarred ? android.R.drawable.star_big_on : android.R.drawable.star_big_off);

            return rowView;
        }
    }

    //Cross out the line
    private void strikeThrough(Todo todo, TextView tvFirstLine){
            if(todo.isDone()){
                tvFirstLine.setPaintFlags(tvFirstLine.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        }
    }

    //Check the date today or not
    private void ifToday(Todo todo, TextView tvSecondLine){
        //Current date and converting String format
        Date currentDate = new Date();
        String currentDateString = dateFormat.format(currentDate);

        //To-do due date
        String todoDateString = dateFormat.format(todo.dueDate);

        if(currentDateString.equals(todoDateString)){
            tvSecondLine.setTextColor(Color.RED);

        }
    }
}