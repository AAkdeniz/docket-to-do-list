package com.aca.docket_android;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.CalendarView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Collections;

public class AddEditActivity extends AppCompatActivity {

    EditText etTask;
    CalendarView cvDueDate;
    Switch swIsDone;
    CheckBox cbIsStarred;
    TextView tvDate;
    // non-null when editing
    Todo currTodo;


    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit);

        initializer();

        setCalendarDate();
    }

    private void initializer() {
        etTask = (EditText) findViewById(R.id.etTask);
        cvDueDate = (CalendarView) findViewById(R.id.cvDueDate);
        swIsDone = (Switch) findViewById(R.id.swIsDone);
        cbIsStarred = (CheckBox) findViewById(R.id.cbIsStarred);
        tvDate = (TextView) findViewById(R.id.tvDate);

        Intent intent = getIntent();
        int index = intent.getIntExtra(MainActivity.EXTRA_INDEX, -1);

        //If the index not -1, get the index of the to-do, if it is -1 it means the to-do is null not created
        currTodo = (index == -1) ? null : MainActivity.todoList.get(index);

        //If currTodo is not null set the values
        if (currTodo != null) {
            etTask.setText(currTodo.title);
            cvDueDate.setDate(currTodo.dueDate.getTime());
            swIsDone.setChecked(currTodo.isDone);
            cbIsStarred.setChecked(currTodo.isStarred);
        }
    }

    private void setCalendarDate() {
            cvDueDate.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
                @Override
                public void onSelectedDayChange(@NonNull CalendarView calendarView, int i, int i1, int i2) {
                    String date = i + "-" + (i1 + 1) + "-" + i2;
                    tvDate.setText(date);
                }
            });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(currTodo == null ? R.menu.add_menu : R.menu.edit_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mi_add: {
                String task = etTask.getText().toString();

                Date dueDate = null;
                dueDate = getCalendarDay(dueDate);
                boolean isDone = swIsDone.isChecked();
                boolean isStarred = cbIsStarred.isChecked();

                Todo todo = new Todo(task, dueDate, isDone, isStarred);
                MainActivity.todoList.add(todo);
                finish();
                return true;
            }
            case R.id.mi_save: {
                currTodo.title = etTask.getText().toString();
                currTodo.dueDate = null;
                currTodo.dueDate = getCalendarDay(currTodo.dueDate);

                currTodo.isDone = swIsDone.isChecked();
                currTodo.isStarred = cbIsStarred.isChecked();
                finish();
                return true;
            }
            case R.id.mi_delete: {
                MainActivity.todoList.remove(currTodo);
                finish();
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private Date getCalendarDay(Date date ){
            try {
                date = dateFormat.parse(tvDate.getText().toString());
            } catch (ParseException e) {
                e.printStackTrace();
                date = new Date (cvDueDate.getDate());
                Log.e("Add", "Error parsing date from String", e);
            }
        return date;
    }
}