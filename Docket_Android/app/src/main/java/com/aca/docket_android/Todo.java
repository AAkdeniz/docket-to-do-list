package com.aca.docket_android;

import android.support.annotation.NonNull;

import java.util.Comparator;
import java.util.Date;


public class Todo implements Comparator<Todo> {
    String title;
    Date dueDate;
    boolean isDone;
    boolean isStarred;

    public Todo(String title, Date dueDate, boolean isDone, boolean isStarred) {
        this.title = title;
        this.dueDate = dueDate;
        this.isDone = isDone;
        this.isStarred = isStarred;
    }

    @Override
    public String toString() {
        return "Todo{" +
                "title='" + title + '\'' +
                ", dueDate=" + dueDate +
                ", isDone=" + isDone +
                ", isStarred=" + isStarred +
                '}';
    }

    public boolean isStarred() {
        return isStarred;
    }

    public void setStarred(boolean starred) {
        isStarred = starred;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    @Override
    public int compare(Todo todo1, Todo todo2) {
        return Boolean.compare(todo2.isStarred, todo1.isStarred);
    }
}
