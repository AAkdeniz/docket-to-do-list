//
//  ToDo.swift
//  Docket_iOS
//
//  Created by mobileapps on 2019-03-28.
//  Copyright © 2019 mobileapps. All rights reserved.
//

import Foundation
import UIKit

struct ToDo : Codable {
    
    var title: String
    var note: String?
    var dueDate: String?
    var isDone: Bool
    var isStarred: Bool
    
    
    // Create a method updateDueDateLabel and call it in viewDidLoad in DetailViewController
    // updateDueDateLabel(date: dueDatePickerView.date)
    //--------------------------------------------------------------------
    
    //MARK: 1-4- Read from Plist
    // Retrieves the array of items stored on disk and returns them if the disk contains any items
    // You can use the FileManager class to locate your app's Documents directory,
    // create a subfolder for archiving data, and store that path to a constant.
    static let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    // create todoList.aca
    static let archiveURL = documentDirectory.appendingPathComponent("todoList").appendingPathExtension("aca")
    
    static func loadToDoList() -> [ToDo]?  {
        guard let codedToDos = try? Data(contentsOf: archiveURL) else {return nil}
        let propertyListDecoder = PropertyListDecoder()
        return try? propertyListDecoder.decode(Array<ToDo>.self, from: codedToDos)
    }
    
    //MARK: 1-5- Write to Plist
    static func saveToDoList(_ todos: [ToDo]) {
        let propertyListEncoder = PropertyListEncoder()
        let codedToDos = try? propertyListEncoder.encode(todos)
        // .noFileProtection option allows the file to be overwritten in the future
        try? codedToDos?.write(to: archiveURL, options: .noFileProtection)
    }
    // Add saveToDos at the end of below methods at TableViewController
    // commit editingStyle
    // unwind
    // completeButtonTapped
    //--------------------------------------------------------------------
}

