//
//  ToDoTableViewCell.swift
//  Docket_iOS
//
//  Created by mobileapps on 2019-03-28.
//  Copyright © 2019 mobileapps. All rights reserved.
//

//The core purpose of the delegate pattern is to allow an object to communicate back to its owner in a decoupled way.
@objc protocol ToDoCellDelegate: class {
    
    // Passing delegator (current object) to delegatee (Table View)
    // Our cell wants to know its indexPath, but only TableView has this ability NOT the cell itself, so
    // cell delegats this TableView
    // Delegator (In this case our selected cell) pass inself to delegatee, so
    // Delegatee can find out delegator indexPath
    func checkmarkTapped(sender: ToDoTableViewCell)
    func starmarkTapped(sender: ToDoTableViewCell)
}

import Foundation
import UIKit

class ToDoTableViewCell: UITableViewCell {

    @IBOutlet weak var checkBoxButton: UIButton!
    @IBOutlet weak var starButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    let dateFormatter = DateFormatter()

    var isDone: Bool?
    var isStarred: Bool?
    var isToday = false
    
    var delegate: ToDoCellDelegate?
    
    func update (with todo: ToDo) {
     titleLabel.text = todo.title
     dateLabel.text = todo.dueDate
     isDone = todo.isDone
     isStarred = todo.isStarred
     isDoneState()
     isStarredStatus()
        
       if let dateLabel = dateLabel.text  {
        var string = dateLabel.components(separatedBy: ",")
        let date: String = string[0]
    
        checkDay(today: today(), date: date)
        }
    }
   
    //Based on isDone state select the image and cross out the line
    func isDoneState(){
        if isDone ?? false{
            checkBoxButton.setImage( UIImage(named: "checked-box"), for: .normal)
            doCrossOutLine()
        }
        else {
            checkBoxButton.setImage( UIImage(named: "blank-check-box"), for: .normal)
            cancelCrossOutLine()
        }
    }
    
    //Based on isStarred state select the image
    func isStarredStatus(){
        if isStarred ?? false{
            starButton.setImage( UIImage(named: "star"), for: .normal)
        }
        else {
            starButton.setImage( UIImage(named: "blank-star"), for: .normal)
        }
    }
    
    //Cross out the line
    func doCrossOutLine() {
        let attributes = [NSAttributedString.Key.foregroundColor : UIColor.red]
        let attributeString =  NSMutableAttributedString(string: titleLabel.text ?? "", attributes:attributes)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle,
                                     value: NSUnderlineStyle.single.rawValue,
                                     range: NSMakeRange(0, attributeString.length))
        self.titleLabel.attributedText = attributeString
    }
    
    //Cancel cross out the line
    func cancelCrossOutLine() {
        let attributeString =  NSMutableAttributedString(string: titleLabel.text ?? "")
        self.titleLabel.attributedText = attributeString
    }
    
    //Basedon the date, change the color
    func checkDay(today: String, date: String ){
        if today == date{
        dateLabel.textColor = UIColor.orange
        } else {
        dateLabel.textColor = UIColor.black
        }
    }
   
    //Get the date of today as String by using DateFormatter
    func today()->String{
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateStyle = DateFormatter.Style.short
      
        let result = formatter.string(from: date)
        return result
    }
    
    
    @IBAction func checkIsDone(_ sender: UIButton) {
         delegate?.checkmarkTapped(sender: self)
         isDoneState()
    }
    
    
    @IBAction func checkIsStarred(_ sender: UIButton) {
        delegate?.starmarkTapped(sender: self)
        isStarredStatus()
    }
}

//Strike through the line -Cross out the line-
extension String {
    func strikeThrough() -> NSAttributedString {
        let attributeString =  NSMutableAttributedString(string: self)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSUnderlineStyle.single.rawValue, range: NSMakeRange(0,attributeString.length))
        return attributeString
    }
}


