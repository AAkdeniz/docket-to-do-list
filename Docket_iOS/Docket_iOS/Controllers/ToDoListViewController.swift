//
//  ToDoListViewController.swift
//  Docket_iOS
//
//  Created by mobileapps on 2019-03-28.
//  Copyright © 2019 mobileapps. All rights reserved.
//

import UIKit

class ToDoListViewController: UIViewController, ToDoCellDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var editButton: UIBarButtonItem!
    @IBOutlet weak var addButton: UIBarButtonItem!
    
    var todoList = [ToDo]()
    var filteredToDos = [ToDo]()
    
    var isSearching = false
    var isToday = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.delegate = self
        tableView.dataSource = self
        tableView.delegate = self
        
      
        setNavigationTitle() 
        // Call load
        if let savedToDoList = ToDo.loadToDoList() {
            todoList = savedToDoList
        }
    }
    
    //When a todo is starred reload the table view
    func reloadTableView() {
        todoList.sort { ($0.isStarred) && !($1.isStarred) }
        tableView.reloadData()
    }
    
    func setNavigationTitle() {
        navigationItem.title = "Docket: To-Do List"
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }

    
    @IBAction func editButtonTapped(_ sender: UIBarButtonItem) {
    
    tableView.setEditing(!tableView.isEditing, animated: true)
    
    //Call save() function to save after editing
     ToDo.saveToDoList(todoList)
}


 func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
    return .delete
}

// Override to support editing the table view.
 func tableView(_ tableView: UITableView,
                        commit editingStyle: UITableViewCell.EditingStyle,
                        forRowAt indexPath: IndexPath) {
    
    if editingStyle == .delete {
        todoList.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .automatic)
        
        //Call saveToDoList() function to save after deleting
        ToDo.saveToDoList(todoList)
    }
}

// moveRowAt to support rearranging the table view.
 func tableView(_ tableView: UITableView,
                        moveRowAt fromIndexPath: IndexPath,
                        to: IndexPath) {
    let movedTodo = todoList.remove(at: fromIndexPath.row)
    
    todoList.insert(movedTodo, at: to.row)
}
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editToDo" {
            
            let indexPath = self.tableView.indexPathForSelectedRow!
            let selectedToDo = todoList[indexPath.row]
            let nVC = segue.destination as! UINavigationController
            let detailViewController = nVC.viewControllers.first as! DetailTableViewController
            
             detailViewController.todo = selectedToDo
        }
    }
    
    @IBAction func unwindToTodoVC(_ unwindSegue: UIStoryboardSegue) {
        
        if unwindSegue.identifier == "saveUnwind" {
            let sourceViewController = unwindSegue.source as! DetailTableViewController
            
            if let todo = sourceViewController.todo {
                
                if let selectedIndexPath = tableView.indexPathForSelectedRow {
                    todoList[selectedIndexPath.row] = todo
                    tableView.reloadRows(at: [selectedIndexPath], with: .none)
            
                } else {
                    let newIndexPath = IndexPath(row: todoList.count, section: 0)
                    
                    todoList.append(todo)
                    tableView.insertRows(at: [newIndexPath], with: .automatic)
                }
            }
        }
        // Save updated Model in aca
        ToDo.saveToDoList(todoList)
    }
    

    //MARK: CheckMark Delegate method ------------------------------------
    func checkmarkTapped(sender: ToDoTableViewCell) {
        
        // Get the indexPath of passed cell
        if let indexPath = tableView.indexPath(for: sender) {
            
            // Reference to current Model object or selected Row .........
            var todo = todoList[indexPath.row]
            
            // Tuggle the state of isComplete attribute in Model Object ..
            todo.isDone = !todo.isDone
            
            // Assign updated Model Object to same location in array Model
            todoList[indexPath.row] = todo
            //............................................................
            
            // Update TableView row
            tableView.reloadRows(at: [indexPath], with: .automatic)
            
            // Save updated array
            ToDo.saveToDoList(todoList)
        }
        
    }
    
    //MARK: StarMark Delegate method ------------------------------------
    func starmarkTapped(sender: ToDoTableViewCell) {
        // Get the indexPath of passed cell
        if let indexPath = tableView.indexPath(for: sender) {
            
            // Reference to current Model object or selected Row .........
            var todo = todoList[indexPath.row]
            
            // Tuggle the state of isComplete attribute in Model Object ..
            todo.isStarred = !todo.isStarred
            
            // Assign updated Model Object to same location in array Model
            todoList[indexPath.row] = todo
            //............................................................
            
            // Update TableView row
            tableView.reloadRows(at: [indexPath], with: .automatic)
            
            reloadTableView()
            
            // Save updated array
            ToDo.saveToDoList(todoList)
        }
    }
}

extension ToDoListViewController: UITableViewDelegate, UITableViewDataSource {
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ToDoCell", for: indexPath) as! ToDoTableViewCell
        
        var todo: ToDo
        
        if isSearching {
            todo = filteredToDos[indexPath.row]
            cell.update(with: todo)
            print("searching")
            
        } else {
           todo = todoList[indexPath.row]
           cell.update(with: todo)
        }
        
        // Activate delegate design pattern in cell
        cell.delegate = self
        
        // Change the state of checkBoxButton based on Model state
       cell.checkBoxButton.isSelected = todo.isDone
        
        // Change the state of starButton based on Model state
       cell.starButton.isSelected = todo.isStarred
        
        return cell
    }

    //To specify the number of rows in each section use:
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching {
                   return filteredToDos.count
        }
        return todoList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

//Search
extension ToDoListViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        filteredToDos = todoList.filter({$0.title.prefix(searchText.count) == searchText})
        
        if searchText.count > 0 {
        isSearching = true
        editButton.isEnabled = false
        addButton.isEnabled = false
        }
        else {
        isSearching = false
        editButton.isEnabled = true
        addButton.isEnabled = true
        }
        
        tableView.reloadData()
    }
}

