//
//  DetailTableViewController.swift
//  Docket_iOS
//
//  Created by mobileapps on 2019-03-28.
//  Copyright © 2019 mobileapps. All rights reserved.
//

import UIKit

class DetailTableViewController: UITableViewController {
    
    @IBOutlet weak var todoTextField: UITextField!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var noteTextView: UITextView!
    @IBOutlet weak var dateLabel: UILabel!
    
    let dateFormatter = DateFormatter()
    
    var isDateSelected = true
    var todo: ToDo?

    override func viewDidLoad() {
        super.viewDidLoad()
        datePicker.isHidden = true

        setNavigationTitle()
        
        if let todo = todo {
            todoTextField.text = todo.title
            dateLabel.text = todo.dueDate
            noteTextView.text = todo.note
            dateButtonState()
        } else {
            dateButtonState()
        }
    }

    func dateButtonState() {
        if dateLabel.text == "" || dateLabel.text == "Not Setted" ||  dateLabel.text == nil{
            cancelButton.isHidden = true
        }else{
            cancelButton.isHidden = false
        }
    }
    
    func setNavigationTitle() {
        navigationItem.title = "To-Do Detail"
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }


override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    super.prepare(for: segue, sender: sender)
    guard segue.identifier == "saveUnwind" else {return}
    
    let title = todoTextField.text ?? "No title"
    var date = dateLabel?.text ?? ""
    if dateLabel.text == "Not Setted"{
        date = ""
    }
    let note = noteTextView.text ?? nil
    
    todo = ToDo(title: title, note: note, dueDate: date, isDone: false, isStarred: false)
}
    
    @IBAction func datePickerChanged(_ sender: UIDatePicker) {
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.short
        
        dateLabel.text = dateFormatter.string(from: datePicker.date)
        
        dateLabel.isHidden = false
        
        if let date = dateFormatter.date(from: dateLabel.text ?? "not setted"){
        if  Calendar.current.isDateInToday(date){
            print("Today")
            print(date)
            }
        }
    }
    
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        cancelButton.isHidden = true
        dateLabel.text = "Not Setted"
    }
    
    //MARK: 5- didSelectRowAt to change cell state -------------------------------
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("------------------ \(indexPath)")
        switch (indexPath) {
        case [2,0]:
           isDateSelected = !isDateSelected
            
            dateLabel.textColor = isDateSelected ? .black : tableView.tintColor
           if cancelButton.isHidden{
              datePicker.isHidden = false
           }
           else{
             datePicker.isHidden = true
        }
            /*
             Call this method if you want subsequent insertions, deletion, and selection operations (for example, cellForRow(at:)
             and indexPathsForVisibleRows) to be animated simultaneously. You can also use this method followed by the endUpdates()
             method to animate the change in the row heights without reloading the cell.
             */
            tableView.beginUpdates()
            tableView.endUpdates()
        case [3,0]:
            isNoteHidden = !isNoteHidden
            
            tableView.beginUpdates()
            tableView.endUpdates()
        default: break
        }
    }
    
    //MARK: 6- Hide and Show DatePicker and Note By Changing Cell Height -
   
    var isNoteHidden = true
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let normalCellHeight = CGFloat(44)
        let largeCellHeight = CGFloat(200)
        
        switch(indexPath) {
        case [2,0]: //Due Date Cell
            return isDateSelected ? normalCellHeight : largeCellHeight
            
        case [3,0]: //Notes Cell
            return isNoteHidden ? normalCellHeight : largeCellHeight
            
        default: return normalCellHeight
        }
    }
}
