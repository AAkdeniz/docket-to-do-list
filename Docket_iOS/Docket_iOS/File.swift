//
//  File.swift
//  Docket_iOS
//
//  Created by mobileapps on 2019-03-30.
//  Copyright © 2019 mobileapps. All rights reserved.
//

import Foundation


public class File{
    private init(){}
  
    static let shared = File()
    
    var todoList = [ToDo]()
        
//        [ToDo(title: "Watch movie a", note: nil, dueDate: nil, isDone: false, isStarred: false),
//                    ToDo(title: "Watch movie b",note: nil, dueDate: nil, isDone: false, isStarred: false)]
    
    var checkedList = [ToDo]()
    var starredList = [ToDo]()
    
  
}
